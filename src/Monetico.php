<?php

namespace Agencecolibri\Monetico;

use Agencecolibri\Monetico\Collections\Currency;
use Agencecolibri\Monetico\Collections\Language;
use Agencecolibri\Monetico\Request\OrderContext;
use Agencecolibri\Monetico\Request\PaymentRequest;
use Agencecolibri\Monetico\Request\OrderContextClient;
use Agencecolibri\Monetico\Request\OrderContextBilling;

class Monetico
{

    const PAIEMENT_URL =   'https://p.monetico-services.com';

    const API_URL =        'https://payment-api.e-i.com/';

    const CAPTURE =        'capture_paiement.cgi';

    const RECREDIT =       'recredit_paiement.cgi';

    const PAIEMENT =       'paiement.cgi';

    const TEST_URL =       'test';

    private $tpe = null;

    private $societe = null;

    private $key = null;

    private $mode = true;

    private PaymentRequest $paymentRequest;

    /**
     * @param string $tpe
     * @param string $societe
     * @param string $key
     * @param bool $mode true = production, false = test
     */
    public function __construct(
        string $tpe,
        string $societe,
        string $key,
        bool $mode = true
    ) {
        $this->tpe = $tpe;
        $this->societe = $societe;
        $this->key = $key;
        $this->mode = $mode;
    }

    public function fastPaiement(
        string $reference,
        float $pricing,
        OrderContextBilling $billing,
        OrderContextClient $client,
        string $url_retour_ok = null,
        string $url_retour_err = null,
        string $description = "",
        string $currency = Currency::EUR,
        string $language = Language::FR,
        ?string $aliasCb = null
    ) : void {

        $context = new OrderContext($billing);
        $context->setOrderContextClient($client);
        $paymentRequest = new PaymentRequest($this->tpe, $this->societe, $this->key, $reference, $pricing, $currency, $language, $context);
        $paymentRequest->setTexteLibre($description);
        if (!is_null($url_retour_ok)) {
            $paymentRequest->setUrlRetourOk($url_retour_ok . '?reference=' . $reference);
        }
        if (!is_null($url_retour_err)) {
            $paymentRequest->setUrlRetourErreur($url_retour_err . '?reference=' . $reference);
        }
        $paymentRequest->setMail($client->getEmail());

        if (!is_null($aliasCb)) {
            $paymentRequest->setAliasCb($aliasCb);
        }
        
        $this->setPaymentRequest($paymentRequest);
    }

    public function getUrlRecredit() : string {

        return self::API_URL . ($this->mode ? '' : '/' .  self::TEST_URL) . '/' . self::RECREDIT;
    }

    public function getUrlCapture() : string {

        return self::API_URL . ($this->mode ? '' : '/' .  self::TEST_URL) . '/' . self::CAPTURE;
    }

    public function getUrlPaiement() : string {

        return self::PAIEMENT_URL . ($this->mode ? '' : '/' .  self::TEST_URL) . '/' . self::PAIEMENT;
    }

    public function getTpe(): string
    {
        return $this->tpe;
    }

    public function getSociete(): string
    {
        return $this->societe;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getPaymentRequest(): ?PaymentRequest
    {
        return $this->paymentRequest;
    }

    public function setPaymentRequest(?PaymentRequest $paymentRequest)
    {
        $this->paymentRequest = $paymentRequest;

        return $this;
    }
}